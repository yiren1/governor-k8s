docker build -t jamesren11/core-client:latest -t jamesren11/core-client:$GIT_SHA -f ./client/Dockerfile ./client
docker build -t jamesren11/core-api:latest -t jamesren11/core-api:$GIT_SHA -f ./api/Dockerfile ./api
docker build -t jamesren11/core-worker:latest -t jamesren11/core-worker:$GIT_SHA -f ./worker/Dockerfile ./worker

docker push jamesren11/core-client:$GIT_SHA
docker push jamesren11/core-api:$GIT_SHA
docker push jamesren11/core-worker:$GIT_SHA

docker push jamesren11/core-client:latest
docker push jamesren11/core-api:latest
docker push jamesren11/core-worker:latest

kubectl apply -f k8s

kubectl set image deployments/api-deployment api=jamesren11/core-api:$GIT_SHA
kubectl set image deployments/client-deployment client=jamesren11/core-client:$GIT_SHA
kubectl set image deployments/worker-deployment worker=jamesren11/core-worker:$GIT_SHA

